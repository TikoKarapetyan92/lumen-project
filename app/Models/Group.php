<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * @var string
     */
    protected $table='group';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'faculty_id'];

    public function faculty (){
        return $this->belongsTo('App\Models\Faculty');
    }
}

