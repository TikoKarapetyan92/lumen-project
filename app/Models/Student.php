<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    /**
     * @var string
     */
    protected $table='student';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'last_name', 'phone', 'photo', 'faculty_id', 'group_id', 'course_id'];

    public function faculty(){
        return $this->belongsTo('App\Models\Faculty');
    }

    public function group(){
        return $this->belongsTo('App\Models\Group');
    }

    public function course(){
        return $this->belongsTo('App\Models\Course');
    }
}