<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Faculty extends Model
{
    /**
     * @var string
     */
    protected $table='faculty';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function student(){
        return $this->hasMany('App\Models\Student');
    }
}