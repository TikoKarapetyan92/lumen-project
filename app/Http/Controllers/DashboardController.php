<?php
/**
 * Created by PhpStorm.
 * User: Sony
 * Date: 07.03.2019
 * Time: 21:22
 */

namespace App\Http\Controllers;


use App\Models\Student;

class DashboardController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/dashboard",
     *      tags={"dashboard"},
     *      summary="Get all student",
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent()
     *      ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index()
  {
      $student = Student::with(['faculty','group','course'])->get();

      return response()->json($student, 200);
  }
}