<?php


namespace App\Http\Controllers;


use App\Models\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class GroupController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/group",
     *      tags={"group"},
     *      summary="Get all group",
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent()
     *      ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $group = Group::with('faculty')->get();

        return response()->json($group, 200);
    }


    /**
     * @OA\Post(
     *     path="/api/v1/group",
     *     tags={"group"},
     *     summary="Adds a new group",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="id",
     *                     type="string"
     *                 ),
     *                 @OA\Property(
     *                    type="integer",
     *                    format="int64",
     *                    minimum=1.0
     *                 ),
     *                 example={"name": "Jessica Smith", "faculty_id": 10}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'faculty_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $group = Group::create($request->all());

        return response()->json($group, 201);
    }


    /**
     * @OA\Get(path="/api/v1/group/{id}",
     *   tags={"group"},
     *   summary="Get Group by group id",
     *   description="For valid response try integer IDs. Other values will generated exceptions",
     *   operationId="getGroupById",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="ID of pet that needs to be fetched",
     *     required=true,
     *     @OA\Schema(
     *         type="integer",
     *         format="int64",
     *         minimum=1.0
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="success",
     *   ),
     *   @OA\Response(response=400, description="Invalid ID supplied")
     * )
     */
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $group = Group::find($id);
        } catch (\Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }

        return response()->json($group, 200);
    }


    /**
     * @OA\Put(path="/api/v1/group/{id}",
     *   tags={"group"},
     *   summary="Updated group",
     *   description="",
     *   operationId="updateGroup",
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                    property="faculty_id",
     *                    type="integer",
     *                    format="int64",
     *                    minimum=1.0
     *                 ),
     *                 example={"name": "Jessica Smith", "faculty_id": 10}
     *             )
     *         )
     *     ),
     *      @OA\Parameter(
     *        name="id",
     *        in="path",
     *        description="ID of pet that needs to be fetched",
     *        required=true,
     *        @OA\Schema(
     *         type="integer",
     *         format="int64",
     *         minimum=1.0
     *        )
     *     ),
     *   @OA\Response(response=400, description="Invalid ID supplied"),
     *   @OA\Response(response=404, description="Group not found"),
     * )
     */
    /**
     * Update the specified resource in group.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'faculty_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $group = Group::find($id);
        $group->update($request->all());

        return response()->json($group, 200);
    }


    /**
     * @OA\Delete(path="/api/v1/group/{id}",
     *   tags={"group"},
     *   summary="Delete group by ID",
     *   description="For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors",
     *   operationId="deleteGroup",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     description="ID of the group that needs to be deleted",
     *     @OA\Schema(
     *         type="integer",
     *         format="int64",
     *         minimum=1.0
     *     )
     *   ),
     *   @OA\Response(response=400, description="Invalid ID supplied"),
     *   @OA\Response(response=404, description="Group not found")
     * )
     */
    /**
     * Remove the specified resource from group.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = Group::find($id);
        if (isset($group)) {
            if ($group->delete()) {
                return response()->json('Group deleted successfully', 200);
            }
        }

        return response()->json('Invalid ID supplied', 400);
    }
}