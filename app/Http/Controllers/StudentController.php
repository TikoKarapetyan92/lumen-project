<?php


namespace App\Http\Controllers;


use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class StudentController extends Controller
{

    /**
     * @OA\Get(
     *      path="/api/v1/student",
     *      tags={"student"},
     *      summary="Get all student",
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent()
     *      ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student = Student::with(['faculty', 'group', 'course'])->get();

        return response()->json($student, 200);
    }



    /**
     * @OA\Post(
     *     path="/api/v1/student",
     *     description="",
     *     summary="Add new student",
     *     operationId="uploadFile",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="file to upload",
     *                     property="photo",
     *                     type="file",
     *                     format="file",
     *                 ),
     *                 required={"photo"}
     *             )
     *         )
     *     ),
     *      @OA\Parameter(
     *         description="Name",
     *         in="query",
     *         name="name",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         ),
     *     ),
     *      @OA\Parameter(
     *         description="Last Nname",
     *         in="query",
     *         name="last_name",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         ),
     *     ),
     *      @OA\Parameter(
     *         description="Phone",
     *         in="query",
     *         name="phone",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="ID from faculty",
     *         in="query",
     *         name="faculty_id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="ID ofrom group",
     *         in="query",
     *         name="group_id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="ID from course",
     *         in="query",
     *         name="course_id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         ),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="successful operation",
     *     ),
     *     tags={
     *         "student"
     *     }
     * )
     *
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'faculty_id' => 'required|integer',
            'last_name' => 'required|max:255',
//            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'phone' => 'required|max:255',
            'group_id' => 'required|integer',
            'course_id' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $imageName = time() . '.' . $request->file('photo')->getClientOriginalExtension();


        $destinationPath = "images/";
        $request->file('photo')->move($destinationPath, $imageName);
        $student = $request->all();
        $student['photo'] = $imageName;
        $student = Student::create($student);

        return response()->json($student, 201);
    }


    /**
     * @OA\Get(path="/api/v1/student/{id}",
     *   tags={"student"},
     *   summary="Get Student by student id",
     *   description="For valid response try integer IDs. Other values will generated exceptions",
     *   operationId="getStudentById",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="ID of pet that needs to be fetched",
     *     required=true,
     *     @OA\Schema(
     *         type="integer",
     *         format="int64",
     *         minimum=1.0
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="success",
     *   ),
     *   @OA\Response(response=400, description="Invalid ID supplied")
     * )
     */

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $student = Student::with(['faculty', 'group', 'course'])->find($id);
        } catch (\Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }
        return response()->json($student, 200);
    }


    /**
     * @OA\Post(
     *     path="/api/v1/student/{id}",
     *     description="",
     *     summary="update student",
     *     operationId="uploadFile",
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="file to upload",
     *                     property="photo",
     *                     type="file",
     *                     format="file",
     *                 ),
     *                 @OA\Property(
     *                     description="file to upload",
     *                     property="Name",
     *                     type="string",
     *     format="string",
     *                 ),
     *                 required={"photo"}
     *             )
     *         )
     *     ),
     *    @OA\Parameter(
     *         description="ID from student",
     *         in="query",
     *         name="faculty_id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         ),
     *     ),
     *      @OA\Parameter(
     *         description="Name",
     *         in="query",
     *         name="name",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         ),
     *     ),
     *      @OA\Parameter(
     *         description="Last Nname",
     *         in="query",
     *         name="last_name",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         ),
     *     ),
     *      @OA\Parameter(
     *         description="Phone",
     *         in="query",
     *         name="phone",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="ID from faculty",
     *         in="query",
     *         name="faculty_id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="ID ofrom group",
     *         in="query",
     *         name="group_id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         ),
     *     ),
     *     @OA\Parameter(
     *         description="ID from course",
     *         in="query",
     *         name="course_id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         ),
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="successful operation",
     *     ),
     *     tags={
     *         "student"
     *     }
     * )
     *
     */
    /**
     * Update the specified resource in student.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'faculty_id' => 'required|integer',
            'last_name' => 'required|max:255',
//            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'phone' => 'required|max:255',
            'group_id' => 'required|integer',
            'course_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $student = $request->all();

        if ($request->hasFile('photo')) {
            $imageName = time() . '.' . $request->file('photo')->getClientOriginalExtension();
            $destinationPath = "images/";
            $request->file('photo')->move($destinationPath, $imageName);
            $student['photo'] = $imageName;
        }

        $student = Student::find($id);
        $student->update($student);

        return response()->json($student, 200);
    }


    /**
     * @OA\Delete(path="/api/v1/student/{id}",
     *   tags={"student"},
     *   summary="Delete student by ID",
     *   description="For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors",
     *   operationId="deleteStudent",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     description="ID of the student that needs to be deleted",
     *     @OA\Schema(
     *         type="integer",
     *         format="int64",
     *         minimum=1.0
     *     )
     *   ),
     *   @OA\Response(response=400, description="Invalid ID supplied"),
     *   @OA\Response(response=404, description="Student not found")
     * )
     */
    /**
     * Remove the specified resource from student.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);
        if (isset($student)) {
            if ($student->delete()) {
                return response()->json('Student deleted successfully', 200);
            }
        }

        return response()->json('Invalid ID supplied', 400);
    }


}