<?php


namespace App\Http\Controllers;


use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CourseController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/course",
     *      tags={"course"},
     *      summary="Get all course",
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent()
     *      ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $course = Course::all();

        return response()->json($course, 200);
    }


    /**
     * @OA\Post(
     *     path="/api/v1/course",
     *     tags={"course"},
     *     summary="Adds a new course",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 example={"name": "Jessica Smith"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="OK"
     *     )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $course = Course::create($request->all());

        return response()->json($course, 201);
    }


    /**
     * @OA\Get(path="/api/v1/course/{id}",
     *   tags={"course"},
     *   summary="Get Course by course id",
     *   description="For valid response try integer IDs. Other values will generated exceptions",
     *   operationId="getCourseById",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="ID of pet that needs to be fetched",
     *     required=true,
     *     @OA\Schema(
     *         type="integer",
     *         format="int64",
     *         minimum=1.0
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="success",
     *   ),
     *   @OA\Response(response=400, description="Invalid ID supplied")
     * )
     */
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $course = Course::find($id);
        } catch (\Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }

        return response()->json($course, 200);
    }



    /**
     * @OA\Put(path="/api/v1/course/{id}",
     *   tags={"course"},
     *   summary="Updated course",
     *   description="",
     *   operationId="updateCourse",
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 example={"name": "Jessica Smith"}
     *             )
     *         )
     *     ),
     *      @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="ID of pet that needs to be fetched",
     *     required=true,
     *     @OA\Schema(
     *         type="integer",
     *         format="int64",
     *         minimum=1.0
     *     )
     *   ),
     *   @OA\Response(response=400, description="Invalid ID supplied"),
     *   @OA\Response(response=404, description="Faculty not found"),
     * )
     */
    /**
     * Update the specified resource in course.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $course = Course::find($id);
        $course->update($request->all());

        return response()->json($course, 200);
    }



    /**
     * @OA\Delete(path="/api/v1/course/{id}",
     *   tags={"course"},
     *   summary="Delete course by ID",
     *   description="For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors",
     *   operationId="deleteCourse",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     description="ID of the course that needs to be deleted",
     *     @OA\Schema(
     *         type="integer",
     *         format="int64",
     *         minimum=1.0
     *     )
     *   ),
     *   @OA\Response(response=400, description="Invalid ID supplied"),
     *   @OA\Response(response=404, description="Course not found")
     * )
     */
    /**
     * Remove the specified resource from course.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::find($id);
        if (isset($course)) {
            if ($course->delete()) {
                return response()->json('Course deleted successfully', 200);
            }
        }

        return response()->json('Invalid ID supplied', 400);
    }
}