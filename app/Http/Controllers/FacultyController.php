<?php


namespace App\Http\Controllers;


use App\Models\Faculty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class FacultyController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/v1/faculty",
     *      tags={"faculty"},
     *      summary="Get all faculty",
     *      @OA\Response(
     *          response=200,
     *          description="Success",
     *          @OA\JsonContent()
     *      ),
     * )
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $course = Faculty::all();

        return response()->json($course, 200);
    }


    /**
     * @OA\Post(
     *     path="/api/v1/faculty",
     *     tags={"faculty"},
     *     summary="Adds a new faculty",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string"
     *                 ),
     *                 example={"name": "Jessica Smith"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="OK"
     *     )
     * )
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $faculty = Faculty::create($request->all());

        return response()->json($faculty, 201);
    }


    /**
     * @OA\Get(path="/api/v1/faculty/{id}",
     *   tags={"faculty"},
     *   summary="Get Faculty by faculty id",
     *   description="For valid response try integer IDs. Other values will generated exceptions",
     *   operationId="getFacultyById",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="ID of pet that needs to be fetched",
     *     required=true,
     *     @OA\Schema(
     *         type="integer",
     *         format="int64",
     *         minimum=1.0
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="successful operation",
     *     @OA\Schema(ref="#/components/schemas/Order")
     *   ),
     *   @OA\Response(response=400, description="Invalid ID supplied")
     * )
     */
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $faculty = Faculty::find($id);
        } catch (\Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }

        return response()->json($faculty, 200);
    }



    /**
     * @OA\Put(path="/api/v1/faculty/{id}",
     *   tags={"faculty"},
     *   summary="Updated faculty",
     *   description="",
     *   operationId="updateFaculty",
     *      @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="name",
     *                     type="string",
     *                 ),
     *                 example={"name": "Jessica Smith"}
     *             )
     *         )
     *     ),
     *      @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="ID of pet that needs to be fetched",
     *     required=true,
     *     @OA\Schema(
     *         type="integer",
     *         format="int64",
     *         minimum=1.0
     *     )
     *   ),
     *   @OA\Response(response=400, description="Invalid ID supplied"),
     *   @OA\Response(response=404, description="Faculty not found"),
     * )
     */
    /**
     * Update the specified resource in faculty.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $faculty = Faculty::find($id);
        $faculty->update($request->all());

        return response()->json($faculty, 200);
    }



    /**
     * @OA\Delete(path="/api/v1/faculty/{id}",
     *   tags={"faculty"},
     *   summary="Delete faculty by ID",
     *   description="For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors",
     *   operationId="deleteFaculty",
     *   @OA\Parameter(
     *     name="id",
     *     in="path",
     *     required=true,
     *     description="ID of the faculty that needs to be deleted",
     *     @OA\Schema(
     *         type="integer",
     *         format="int64",
     *         minimum=1.0
     *     )
     *   ),
     *   @OA\Response(response=400, description="Invalid ID supplied"),
     *   @OA\Response(response=404, description="Faculty not found")
     * )
     */
    /**
     * Remove the specified resource from faculty.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faculty = Faculty::find($id);
        if (isset($faculty)) {
            if ($faculty->delete()) {
                return response()->json('Faculty deleted successfully', 200);
            }
        }

        return response()->json('Invalid ID supplied', 400);
    }
}