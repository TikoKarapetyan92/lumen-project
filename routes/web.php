<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix'=>'api/v1'], function() use($router){
    $router->group(['prefix'=>'/course'], function() use($router) {
        $router->get('/', 'CourseController@index');
        $router->post('/', 'CourseController@create');
        $router->get('/{course}', 'CourseController@show');
        $router->put('/{course}', 'CourseController@update');
        $router->delete('/{course}', 'CourseController@destroy');
    });
    $router->group(['prefix'=>'/faculty'], function() use($router) {
        $router->get('/', 'FacultyController@index');
        $router->post('/', 'FacultyController@create');
        $router->get('/{faculty}', 'FacultyController@show');
        $router->put('/{faculty}', 'FacultyController@update');
        $router->delete('/{faculty}', 'FacultyController@destroy');
    });
    $router->group(['prefix'=>'/group'], function() use($router) {
        $router->get('/', 'GroupController@index');
        $router->post('/', 'GroupController@create');
        $router->get('/{group}', 'GroupController@show');
        $router->put('/{group}', 'GroupController@update');
        $router->delete('/{group}', 'GroupController@destroy');
    });
    $router->group(['prefix'=>'/student'], function() use($router) {
        $router->get('/', 'StudentController@index');
        $router->post('/', 'StudentController@create');
        $router->get('/{student}', 'StudentController@show');
        $router->post('/{student}', 'StudentController@update');
        $router->delete('/{student}', 'StudentController@destroy');
    });
    $router->group(['prefix'=>'/dashboard'], function() use($router) {
        $router->get('/', 'DashboardController@index');
    });
});

